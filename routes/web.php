<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => [ 'web', 'auth' ], 'prefix' => 'freems'], function () {
  Route::resource('/products', 'Admin\ProductController', [
    'names' => [
      'index' => 'admin.products.index',
      'create' => 'admin.products.create',
      'store' => 'admin.products.store',
      'edit' => 'admin.products.edit',
      'update' => 'admin.products.update',
      'destroy' => 'admin.products.delete'
    ]
  ]);

  Route::resource('/product-inventory', 'Admin\ProductInventoryController', [
    'names' => [
      'index' => 'admin.product-inventory.index',
      'create' => 'admin.product-inventory.create',
      'store' => 'admin.product-inventory.store',
      'edit' => 'admin.product-inventory.edit',
      'update' => 'admin.product-inventory.update',
      'destroy' => 'admin.product-inventory.delete'
    ]
  ]);
});