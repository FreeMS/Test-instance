<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('shops', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();

      $table->string('title');
      $table->string('slug');
      $table->text('sub_title');
      $table->string('cover');
      $table->string('cover_color');

      $table->unsignedInteger('influencer_id');
      $table->foreign('influencer_id')->references('id')->on('influencers')->onDelete('cascade');
    });

    Schema::create('shop_products', function (Blueprint $table) {
      $table->unsignedInteger('shop_id');
      $table->unsignedInteger('product_id');
      $table->primary(['shop_id', 'product_id']);
      $table->timestamps();

      $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
      $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('shops');
  }
}
