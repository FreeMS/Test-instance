<?php

use FreeMS\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Str;

/**
 * Class CreateRightsAndRolesTable
 */
class CreateRightsAndRolesTables extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('rights', function (Blueprint $table) {
      $table->increments('id');
      $table->string('right_key');
      $table->text('right_description');
      $table->timestamps();
    });

    Schema::create('roles', function (Blueprint $table) {
      $table->increments('id');
      $table->string('role_key', 128)->nullable()->default(null);
      $table->unique('role_key');
      $table->text('role_description');
      $table->timestamps();
    });

    Schema::create('role_rights', function (Blueprint $table) {
      $table->unsignedInteger('role_id');
      $table->unsignedInteger('right_id');
      $table->primary(['role_id', 'right_id']);
      $table->timestamps();

      $table->foreign('role_id')->references('id')->on('roles')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->foreign('right_id')->references('id')->on('rights')
        ->onUpdate('cascade')->onDelete('cascade');
    });

    Schema::create('user_roles', function (Blueprint $table) {
      $table->unsignedInteger('user_id');
      $table->unsignedInteger('role_id');
      $table->primary(['user_id', 'role_id']);
      $table->timestamps();

      $table->foreign('user_id')->references('id')->on('users')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->foreign('role_id')->references('id')->on('roles')
        ->onUpdate('cascade')->onDelete('cascade');
    });

    Schema::create('user_rights', function (Blueprint $table) {
      $table->unsignedInteger('user_id');
      $table->unsignedInteger('right_id');
      $table->primary(['user_id', 'right_id']);
      $table->timestamps();

      $table->foreign('user_id')->references('id')->on('users')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->foreign('right_id')->references('id')->on('rights')
        ->onUpdate('cascade')->onDelete('cascade');
    });

    $this->populateTableDatas();
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('user_rights');
    Schema::drop('user_roles');
    Schema::drop('role_rights');
    Schema::drop('roles');
    Schema::drop('rights');
  }


  /**
   * @var array
   */
  private $data = [
    'rights' => [
      /* Users */
      [ 'right_key' => 'users.view', 'right_description' => 'Users - view' ],
      [ 'right_key' => 'users.manage', 'right_description' => 'Users - manage' ]
    ],

    'roles' => [
      [
        'role_key' => 'administrator',
        'role_description' => 'Super Admin',
        '__rights_keys__' => [
          'users.view', 'users.manage'
        ],
      ],
      [
        'role_key' => 'editor',
        'role_description' => 'Editor',
        '__rights_keys__' => [],
      ],
    ],

  ];

  /**
   *
   */
  private function populateTableDatas()
  {
    $rightKeyIdMap = [];
    foreach ($this->data['rights'] as $right) {
      $rightId = \DB::table('rights')->insertGetId(
        array_merge($right, [
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
        ])
      );
      $rightKeyIdMap[$right['right_key']] = $rightId;
    }

    foreach ($this->data['roles'] as $role) {
      $roleAttributes = array_only($role, ['role_key', 'role_description']);
      $roleId = \DB::table('roles')->insertGetId(
        array_merge($roleAttributes, [
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
        ])
      );

      foreach ($role['__rights_keys__'] as $roleRightKey) {
        $rightId = $rightKeyIdMap[$roleRightKey];
        \DB::table('role_rights')->insert([
          'role_id' => $roleId,
          'right_id' => $rightId,
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
        ]);
      }
    }

    $user = new User([
      'name' => 'Ucha Gviniashvili',
      'email' => 'ucha4964@gmail.com',
      'password' => bcrypt('aaaaaas'),
      'remember_token' => Str::random(32)
    ]);
    $user->save();
    $user->roles()->attach([1]);

    $user = new User([
      'name' => 'Gio Mosashvili',
      'email' => 'gio.mosashvili1@gmail.com',
      'password' => bcrypt('123456'),
      'remember_token' => Str::random(32)
    ]);
    $user->save();
    $user->roles()->attach([1]);

    $user = new User([
      'name' => 'Nika Chitanava',
      'email' => 'chitanava@gmail.com',
      'password' => bcrypt('123456'),
      'remember_token' => Str::random(32)
    ]);
    $user->save();
    $user->roles()->attach([1]);

    $user = new User([
      'name' => 'David Savaneli',
      'email' => 'david_savaneli@yahoo.com',
      'password' => bcrypt('123456'),
      'remember_token' => Str::random(32)
    ]);
    $user->save();
    $user->roles()->attach([1]);
  }
}
