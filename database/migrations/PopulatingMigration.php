<?php
use Illuminate\Database\Migrations\Migration;

abstract class PopulatingMigration extends Migration
{
  protected function populate($populations)
  {
    foreach ($populations as $population) {
      foreach ($population['rows'] as $row) {
        if ($population['withTimestamps']) {
          $row['created_at'] = isset($row['created_at']) ? $row['created_at'] : \Carbon\Carbon::now()->toRfc822String();
          $row['updated_at'] = isset($row['updated_at']) ? $row['updated_at'] : \Carbon\Carbon::now()->toRfc822String();
        }
        \DB::table($population['table'])->insert($row);
      }
    }
  }
}