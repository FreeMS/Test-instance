<?php

require_once 'PopulatingMigration.php';
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends PopulatingMigration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('size_standards', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
    });

    Schema::create('currencies', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->float('rate');
      $table->boolean('is_base');
    });

    Schema::create('weight_units', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
    });

    Schema::create('product_colors', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();

      $table->string('name');
      $table->string('code');
    });

    Schema::create('products', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();

      $table->string('name');
      $table->string('slug')->unique();
      $table->text('description');
      $table->boolean('enabled')->default(true);

      $table->unsignedInteger('size_standard_id');
      $table->unsignedInteger('currency_id');
      $table->unsignedInteger('weight_unit_id');

      $table->unsignedInteger('user_id')->nullable();
      $table->unsignedInteger('brand_id');

      $table->foreign('size_standard_id')->references('id')->on('size_standards')->onDelete('cascade')->onUpdate('cascade');
      $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');
      $table->foreign('weight_unit_id')->references('id')->on('weight_units')->onDelete('cascade')->onUpdate('cascade');

      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
      $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade')->onUpdate('cascade');
    });

    Schema::create('product_inventories', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();

      $table->string('size');
      $table->float('price');
      $table->float('stock_price');
      $table->float('weight');
      $table->integer('amount');
      $table->integer('ord');

      $table->unsignedInteger('product_color_id');
      $table->unsignedInteger('product_id');

      $table->foreign('product_color_id')->references('id')->on('product_colors')->onDelete('cascade')->onUpdate('cascade');
      $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');

      $table->index('ord');
    });

    $this->populate($this->populations);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('product_inventories');
    Schema::dropIfExists('products');
    Schema::dropIfExists('product_colors');
    Schema::dropIfExists('weight_units');
    Schema::dropIfExists('currencies');
    Schema::dropIfExists('size_standards');
  }

  protected $populations = [
    [
      'table' => 'size_standards',
      'withTimestamps' => false,
      'rows' => [
        ['name' => 'EU'],
        ['name' => 'UK'],
        ['name' => 'US']
      ]
    ],
    [
      'table' => 'currencies',
      'withTimestamps' => false,
      'rows' => [
        ['name' => 'EUR', 'rate' => 1.00, 'is_base' => true ],
        ['name' => 'GBP', 'rate' => 1.14, 'is_base' => false],
        ['name' => 'USD', 'rate' => 0.80, 'is_base' => false]
      ]
    ],
    [
      'table' => 'weight_units',
      'withTimestamps' => false,
      'rows' => [
        ['name' => 'Pound'],
        ['name' => 'Kg'],
        ['name' => 'Gram'],
        ['name' => 'LBS'],
        ['name' => 'Ounce']
      ]
    ],
    [
      'table' => 'product_colors',
      'withTimestamps' => false,
      'rows' => [
        ['name' => 'White', 'code' => 'ffffff'],
        ['name' => 'Black', 'code' => '000000'],
        ['name' => 'Red', 'code' => 'ff0000'],
        ['name' => 'Green', 'code' => '00ff00'],
        ['name' => 'Blue', 'code' => '0000ff'],
        ['name' => 'Yellow', 'code' => 'ffff00']
      ]
    ]
  ];
}
