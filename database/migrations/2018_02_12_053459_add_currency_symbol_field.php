<?php

use App\Models\Currency;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencySymbolField extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('currencies', function (Blueprint $table) {
      $table->string('symbol')->nullable();
    });

    Currency::where('name', 'EUR')->update([ 'symbol' => '€' ]);
    Currency::where('name', 'USD')->update([ 'symbol' => '$' ]);
    Currency::where('name', 'GBP')->update([ 'symbol' => '£' ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('currencies', function (Blueprint $table) {
      $table->dropColumn('symbol');
    });
  }
}
