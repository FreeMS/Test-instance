<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentFieldsToBrand extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('brands', function (Blueprint $table) {
      $table->string('business_name')->nullable();
      $table->string('registration_number')->nullable();
      $table->string('iban')->nullable();
      $table->string('swift')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('brands', function (Blueprint $table) {
      $table->dropColumn('swift');
      $table->dropColumn('iban');
      $table->dropColumn('registration_number');
      $table->dropColumn('business_name');
    });
  }
}
