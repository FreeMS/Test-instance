<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

require_once 'PopulatingMigration.php';

class CreateFoldersTable extends PopulatingMigration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('folders', function (Blueprint $table) {
      $table->increments('id');

      $table->string('name');
      $table->softDeletes();
      $table->nestedSet();
      $table->timestamps();
    });

    Schema::create('folderables', function (Blueprint $table) {
      $table->unsignedInteger('folder_id');
      $table->unsignedInteger('folderable_id');
      $table->string('folderable_type');
      $table->primary(['folder_id', 'folderable_id', 'folderable_type']);

      $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
    });

    $this->populate([
      [
        'table' => 'folders',
        'withTimestamps' => false,
        'rows' => [
          [ 'name' => '.tmp' ]
        ]
      ]
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('folderables');
    Schema::dropIfExists('folders');
  }
}
