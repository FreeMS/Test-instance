<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Influencer::class, function (Faker $faker) {
  return [
    'instagram' => $faker->name,
    'blog' => $faker->name,
    'followers' => $faker->numberBetween(10000, 100000000)
  ];
});

$factory->define(App\Models\Shop::class, function (Faker $faker) {
  return [
    'title' => $faker->name,
    'sub_title' => $faker->sentence(10),
    'cover' => 'tmp/curator-cover.png', // secret
    'cover_color' => '#682c2a'
  ];
});

$factory->define(App\Models\Brand::class, function (Faker $faker) {
  return [
    'image' => str_replace('public/', '', $faker->image('public/tmp/faker')),
    'name' => $faker->words(2, true),
    'website' => $faker->word,
    'summary' => $faker->sentence(10),
    'registration_token' => Str::random(32),
    'business_name' => $faker->words(2, true),
    'registration_number' => $faker->randomNumber(8),
    'iban' => $faker->iban('SE'),
    'swift' => $faker->swiftBicNumber,
    'registered' => true
  ];
});

$factory->define(App\Models\ProductInventory::class, function (Faker $faker) {
  return [
    'size' => $faker->numberBetween(7, 11),
    'price' => $faker->numberBetween(15, 550),
    'stock_price' => $faker->numberBetween(15, 550),
    'weight' => $faker->numberBetween(0.1, 1.9),
    'amount' => $faker->numberBetween(10, 500),
    'ord' => $faker->numberBetween(0, 10)
  ];
});

$factory->define(App\Models\Product::class, function (Faker $faker) {
  return [
    'name' => $faker->words(3, true),
    'description' => $faker->sentence(50)
  ];
});

$factory->define(App\Models\InstagramFeed::class, function (Faker $faker) {
  return [
    'image' => 'tmp/instagram'.rand(1, 6).'.png',
    'link' => 'https://www.instagram.com/'
  ];
});