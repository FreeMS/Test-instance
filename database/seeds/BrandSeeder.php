<?php

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductInventory;
use App\Models\Shop;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $brands = factory(Brand::class)->times(5)->make();

    $brands->each(function ($brand) {
      $brand->save();

      $products = factory(Product::class)->times(5)->make();

      $products->each(function ($product) use($brand) {
        $product->user()->associate(1);
        $product->brand()->associate($brand);
        $product->size_standard()->associate(rand(1, 3));
        $product->currency()->associate(rand(1, 3));
        $product->weight_unit()->associate(rand(1, 5));

        $product->save();

        $shops = Shop::inRandomOrder()->get();
        $shops->each(function ($shop) use($product) {
          $shop->products()->attach($product);
        });

        $inventory = factory(ProductInventory::class)->times(5)->make();
        $inventory->each(function ($item) use($product) {
          $item->product()->associate($product);
          $item->color()->associate(rand(1, 6));
          $item->save();
        });
      });
    });
  }
}
