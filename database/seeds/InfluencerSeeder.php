<?php

use App\Models\Influencer;
use App\Models\InstagramFeed;
use App\Models\Shop;
use App\User;
use Illuminate\Database\Seeder;

class InfluencerSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $users = factory(User::class)->times(5)->make();

    $users->each(function ($user) {
      $user->save();

      $influencer = factory(Influencer::class)->make();
      $influencer->user()->associate($user);

      $influencer->save();

      $instagramFeed = factory(InstagramFeed::class)->times(6)->make();
      $instagramFeed->each(function ($feed) use($influencer) {
        $feed->influencer()->associate($influencer);
        $feed->save();
      });

      $shop = factory(Shop::class)->make();
      $shop->influencer()->associate($influencer);
      $shop->save();
    });
  }
}
