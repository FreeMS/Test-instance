function initSummernote(cfg) {
  var height = cfg.height || 300;
  var el = cfg.el;
  if(!el || !el.length) throw "Unable to initialize summernote, html element not found";

  var lastSummernoteContext = null;

  var fileManager = createFileManager({
    modal: true,
    fileType: 'image',
    folderId: cfg.folderId,
    indexRoute: cfg.indexRoute,
    uploadBehaviour: 'related',
    onSelect: function (files) {
      $.each(files, function (k, file) {

        var img = document.createElement('img');
        img.src = '/' + file.src;
        img.alt = file.title;
        img.setAttribute('data-fm-association', file.id);

        var picture = document.createElement('picture');

        file.thumbnails.push(file);
        file.thumbnails.sort(function (a, b) {
          return b.width - a.width;
        });

        $.each(file.thumbnails, function (k, thumbnail) {console.log(thumbnail);
          var width = file.thumbnails[k + 1] ? file.thumbnails[k + 1].width : 1;
          var source = document.createElement('source');
          source.setAttribute('srcset', thumbnail.src);
          source.setAttribute('media', '(min-width: ' + width + 'px)');
          picture.appendChild(source);
        });

        picture.appendChild(img);
        lastSummernoteContext.invoke('editor.insertNode', picture);
      });
    }
  });

  var mediaButton = function (context) {
    lastSummernoteContext = context;
    var ui = $.summernote.ui;

    var button = ui.button({
      contents: '<i class="fa fa-image"></i>',
      tooltip: 'Insert Media',
      click: function () {
        fileManager.show();
      }
    });

    return button.render();   // return button as jquery object
  };

  el.summernote({
    height: height,
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['strikethrough', 'superscript', 'subscript']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['mediaButton', ['mediaButton']]
    ],
    buttons: {
      mediaButton: mediaButton
    }
  });
}