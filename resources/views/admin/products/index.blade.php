@extends('freems::common.layout')

@section('content')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-12">
      <h2>Products</h2>
      <ol class="breadcrumb">
        <li>
          <a href="{{ route('freems.dashboard') }}">Dashboard</a>
        </li>
        <li class="active">
          <strong>Products</strong>
        </li>
      </ol>
    </div>
  </div>

  <div class="wrapper wrapper-content">

    @if(\Session::get('status'))
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group">
            <div class="alert alert-success text-center">
              {{ \Session::get('status') }}
            </div>
          </div>
        </div>
      </div>
    @endif

    <div class="row">
      <div class="col-md-6">
        <a href="{{ route('admin.products.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i>
          Add new Product</a>
      </div>
    </div><br>

    <div class="ibox-content m-b-sm border-bottom">
      <form action="{{ route('admin.products.index') }}?">
        <div class="row">
          <div class="col-sm-5">
            <div class="form-group">
              <label class="control-label" for="name">Product Name</label>
              <input type="text" id="name" name="filter[name]" value="{{ @$query['filter']['name'] }}" placeholder="Product Name"
                     class="form-control">
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <label class="control-label" for="size">Size</label>
              <input type="text" id="size" name="filter[size]" value="{{ @$query['filter']['size'] }}" placeholder="Size" class="form-control">
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <label class="control-label" for="color">Color</label>
              <input type="text" id="color" name="filter[color]" value="{{ @$query['filter']['color'] }}" placeholder="Color" class="form-control">
            </div>
          </div>
          {{--<div class="col-sm-2">
            <div class="form-group">
              <label class="control-label" for="status">Status</label>
              <select name="filter[status]" id="status" class="form-control">
                <option value="" {{ !@$query['filter']['status'] ? 'selected' : '' }}>Any</option>
                <option value="1" {{ @$query['filter']['status'] == '1' ? 'selected' : '' }}>Enabled</option>
                <option value="0" {{ @$query['filter']['status'] == '0' ? 'selected' : '' }}>Disabled</option>
              </select>
            </div>
          </div>--}}
          <div class="col-sm-1">
            <div class="form-group">
              <label class="control-label" style="color: transparent">Submit</label>
              <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Search</button>
            </div>
          </div>
        </div>
      </form>
    </div>

    @if(count($inventory) == 0)
      <br><h3 class="text-center">Results not found</h3><br>
    @else
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox">
            <div class="ibox-content">
              <div class="table-responsive">
                <table class="table table-striped" data-page-size="15">
                  <thead>
                  <tr>
                    <th>
                      <a href="javascript:;" class="sortable-column-link" data-column="name" data-value="{{ @$query['sort']['name'] }}" data-url="{{ $full_url }}" style="color: #676a6c">Product Name
                      @if(empty($query['sort']['name']))
                        <i class="fa fa-sort"></i>
                      @elseif($query['sort']['name'] == 'asc')
                          <i class="fa fa-sort-up"></i>
                      @elseif($query['sort']['name'] == 'desc')
                          <i class="fa fa-sort-down"></i>
                      @endif
                      </a>
                    </th>
                    <th width="15%">
                      <a href="javascript:;" class="sortable-column-link" data-column="size" data-value="{{ @$query['sort']['size'] }}" data-url="{{ $full_url }}" style="color: #676a6c">Size</a>
                    </th>
                    <th width="15%">
                      <a href="javascript:;" class="sortable-column-link" data-column="color" data-value="{{ @$query['sort']['color'] }}" data-url="{{ $full_url }}" style="color: #676a6c">Color</a>
                    </th>
                    <th width="15%">
                      <a href="javascript:;" class="sortable-column-link" data-column="price" data-value="{{ @$query['sort']['price'] }}" data-url="{{ $full_url }}" style="color: #676a6c">Price</a>
                    </th>
                    <th width="10%">
                      <a href="javascript:;" class="sortable-column-link" data-column="amount" data-value="{{ @$query['sort']['amount'] }}" data-url="{{ $full_url }}" style="color: #676a6c">Amount
                        @if(empty($query['sort']['amount']))
                          <i class="fa fa-sort"></i>
                        @elseif($query['sort']['amount'] == 'asc')
                          <i class="fa fa-sort-up"></i>
                        @elseif($query['sort']['amount'] == 'desc')
                          <i class="fa fa-sort-down"></i>
                        @endif
                      </a>
                    </th>
                    <th width="10%">Status</th>
                    <th width="10%" class="text-right">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($inventory as $item)
                    <tr>
                      <td>{{ $item->product_name }}</td>
                      <td>{{ $item->sizes ?: '-' }}</td>
                      <td>
                        &nbsp;&nbsp;{{ $item->colors ?: '-' }}
                      </td>
                      <td>{{ $item->currency.' '.$item->min_price.' - '.$item->max_price }}</td>
                      <td>{{ $item->amount ?: '-' }}</td>
                      <td>
                        @if(!$item->enabled || !$item->amount)
                          <span class="label label-danger">Disabled</span>
                        @elseif($item->amount < 100)
                          <span class="label label-warning">Low stock</span>
                        @else
                          <span class="label label-primary">Enabled</span>
                        @endif
                      </td>

                      <td class="text-right">
                        <div class="btn-group btn-group-xs">
                          <a href="{{ route('admin.products.edit', [ $item->product_id ]) }}" title="Update"
                             class="btn btn-primary"><i
                              class="fa fa-pencil"></i></a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="8" class="text-right">
                      {{ $inventory->links() }}
                    </td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endif

      <form action="{{ route('admin.products.delete', [ '' ]) }}" method="DELETE" id="delete-product-form">
        {!! csrf_field() !!}
      </form>
  </div>
@endsection

@push('scripts')
  <script>
    $(function () {
      $('.remove-item').click(function () {
        var form = $('#delete-product-form');
        var productId = $(this).data().id;

        Modal.show({
          yesClass: 'btn-danger',
          body: 'You are going to delete this Product, do you want to continue?',
          yes: 'Delete',
          callback: function (btn) {
            Modal.hide();

            if (btn === 'yes') {
              var action = form.attr('action');
              form.attr({action: action + '/' + productId}).submit();
            }
          }
        });
      });

      $('.sortable-column-link').click(function () {
        var data = $(this).data();
        var url = unescape(data.url);
        var pattern = new RegExp('sort\\[' + data.column + '\\]=' + data.value);

        if(!data.value) {
          if(!url.match(/\?/)) {
            url += '?';
          } else {
            url += '&';
          }

          url += 'sort[' + data.column + ']=asc';
        } else if(data.value === 'asc') {
          url = url.replace(pattern, 'sort[' + data.column + ']=desc');
        } else if(data.value === 'desc') {
          url = url.replace(pattern, '');
        }

        window.location = url;
      });
    });
  </script>
@endpush
