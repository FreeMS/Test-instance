@extends('freems::common.layout')

@section('content')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-12">
      <h2>Brands</h2>
      <ol class="breadcrumb">
        <li>
          <a href="{{ route('freems.dashboard') }}">Dashboard</a>
        </li>
        <li>
          <a href="{{ route('admin.products.index') }}">Products</a>
        </li>
        <li class="active">
          <strong>Add new product</strong>
        </li>
      </ol>
    </div>
  </div>

  <div class="wrapper wrapper-content">
    @if(\Session::get('status'))
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group">
            <div class="alert alert-success text-center">
              {{ \Session::get('status') }}
            </div>
          </div>
        </div>
      </div>
    @endif

    <div class="row">
      <div class="col-xs-12">
        <form action="{{ route('admin.products.store') }}" method="POST" class="form-horizontal form-bordered">
          {!! csrf_field() !!}

          <div class="row">
            <div class="col-xs-12">
              <div class="ibox">
                <div class="ibox-content">
                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label" for="name">Name:</label>
                    <div class="col-md-10">
                      <input type="text" id="name" name="name" class="form-control"
                             value="{{ old('name') }}">
                      <span class="help-block">{{ $errors->first('name') }}</span>
                    </div>
                  </div>

                  <?php $old_images = old('images') ?: []; ?>
                  <div class="form-group{{ $errors->has('images') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label">Image:</label>
                    <div class="col-md-8">
                      <button type="button" class="btn btn-primary select-media-btn">Select Image</button>
                      <div class="selected-image-inputs">
                        @foreach($old_images as $image_id)
                          <input type="hidden" name="images[]" value="{{ $image_id }}">
                        @endforeach
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-2 control-label">Image Preview:</label>
                    <div class="col-md-8">
                      <div class="lightBoxGallery" style="text-align: left;">
                        <div class="image-previews">
                          @foreach($old_images as $image_id)
                            <?php $image = \FreeMS\Models\File::where('id', $image_id)->first(); ?>
                            <a href="{{ $image->src }}" title="{{ $image->title }}" data-gallery="article-images">
                              <img src="{{ $image->src }}" alt="{{ $image->title }}">
                            </a>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label" for="summary">Description:</label>
                    <div class="col-md-10">
                    <textarea style="height: 150px" id="description" name="description"
                              class="form-control">{{ old('description') }}</textarea>
                      <span class="help-block">{{ $errors->first('description') }}</span>
                    </div>
                  </div>

                  <?php $old_brand_id = old('brand_id'); ?>
                  <div class="form-group{{ $errors->has('brand_id') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label" for="brand_id">Brand:</label>
                    <div class="col-md-10">
                      <select type="text" id="brand_id" name="brand_id" class="form-control">
                        @foreach($brands as $brand)
                          <option
                            value="{{ $brand->id }}"{{ $old_brand_id == $brand->id ? ' selected' : '' }}>{{ $brand->name }}</option>
                        @endforeach
                      </select>
                      <span class="help-block">{{ $errors->first('brand_id') }}</span>
                    </div>
                  </div>

                  <?php $old_shop_id = old('shop_id'); ?>
                  <div class="form-group{{ $errors->has('shop_id') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label" for="shop_id">Shop:</label>
                    <div class="col-md-10">
                      <select type="text" id="shop_id" name="shop_id" class="form-control">
                        @foreach($shops as $shop)
                          <option
                            value="{{ $shop->id }}"{{ $old_shop_id == $shop->id ? ' selected' : '' }}>{{ $shop->title }}</option>
                        @endforeach
                      </select>
                      <span class="help-block">{{ $errors->first('shop_id') }}</span>
                    </div>
                  </div>

                  <?php $old_currency_id = old('currency_id'); ?>
                  <div class="form-group{{ $errors->has('currency_id') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label" for="currency_id">Currency:</label>
                    <div class="col-md-10">
                      <select type="text" id="currency_id" name="currency_id" class="form-control">
                        @foreach($currencies as $currency)
                          <option
                            value="{{ $currency->id }}"{{ $old_currency_id == $currency->id ? ' selected' : '' }}>{{ $currency->symbol.' '.$currency->name }}</option>
                        @endforeach
                      </select>
                      <span class="help-block">{{ $errors->first('currency_id') }}</span>
                    </div>
                  </div>

                  <?php $old_size_standard_id = old('size_standard_id'); ?>
                  <div class="form-group{{ $errors->has('size_standard_id') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label" for="size_standard_id">Size standard:</label>
                    <div class="col-md-10">
                      <select type="text" id="size_standard_id" name="size_standard_id" class="form-control">
                        @foreach($size_standards as $size_standard)
                          <option
                            value="{{ $size_standard->id }}"{{ $old_size_standard_id == $size_standard->id ? ' selected' : '' }}>{{ $size_standard->name }}</option>
                        @endforeach
                      </select>
                      <span class="help-block">{{ $errors->first('size_standard_id') }}</span>
                    </div>
                  </div>

                  <?php $old_weight_unit_id = old('weight_unit_id'); ?>
                  <div class="form-group{{ $errors->has('weight_unit_id') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label" for="weight_unit_id">Weight unit:</label>
                    <div class="col-md-10">
                      <select type="text" id="weight_unit_id" name="weight_unit_id" class="form-control">
                        @foreach($weight_units as $weight_unit)
                          <option
                            value="{{ $weight_unit->id }}"{{ $old_weight_unit_id == $weight_unit->id ? ' selected' : '' }}>{{ $weight_unit->name }}</option>
                        @endforeach
                      </select>
                      <span class="help-block">{{ $errors->first('weight_unit_id') }}</span>
                    </div>
                  </div>

                  <div class="form-group form-actions">
                    <div class="col-md-10 col-md-offset-2">
                      <button type="submit" class="btn btn-sm btn-primary">Add</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12">
              <button type="button" class="btn btn-primary add-inventory-btn">
                <i class="fa fa-plus"></i> Add Inventory
              </button>
              <br><br>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12">
              <div class="ibox">
                <div class="ibox-content">
                  <table class="table table-stripped product-inventory-table" data-page-size="15">
                    <thead>
                    <tr>
                      <th width="8.5%">Size</th>
                      <th width="17%">Color</th>
                      <th width="8.5%">Price</th>
                      <th width="17%">Stock price</th>
                      <th width="17%">Weight</th>
                      <th width="17%">Amount</th>
                      <th class="text-right">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="footable-even inventory-row-view-mode template" style="display: none">
                      <td data-prop="size"></td>
                      <td data-prop="color"></td>
                      <td data-prop="price"></td>
                      <td data-prop="stock_price"></td>
                      <td data-prop="weight"></td>
                      <td data-prop="amount"></td>
                      <td class="text-right">
                        <div class="btn-group">
                          <button class="btn-primary btn btn-xs edit-inventory-btn"><i class="fa fa-pencil"></i>
                            Edit
                          </button>
                          <button class="btn-danger btn btn-xs delete-inventory-btn"><i class="fa fa-trash"></i>
                            Delete
                          </button>
                        </div>
                      </td>
                    </tr>

                    <tr class="footable-even inventory-row-edit-mode template" style="display: none">
                      <td>
                        <div class="form-group" style="margin: 0">
                          <input type="text" name="size[]" placeholder="Size" class="form-control">
                        </div>
                      </td>
                      <td>
                        <div class="form-group" style="margin: 0">
                          <select name="color[]" class="form-control">
                            @foreach($colors as $color)
                              <option value="{{ $color->id }}">{{ $color->name }}</option>
                            @endforeach
                          </select>
                        </div>
                      </td>
                      <td>
                        <div class="form-group" style="margin: 0">
                          <input type="text" name="price[]" placeholder="Price" class="form-control">
                        </div>
                      </td>
                      <td>
                        <div class="form-group" style="margin: 0">
                          <input type="text" name="stock_price[]" placeholder="Stock Price" class="form-control">
                        </div>
                      </td>
                      <td>
                        <div class="form-group" style="margin: 0">
                          <input type="text" name="weight[]" placeholder="Weight" class="form-control">
                        </div>
                      </td>
                      <td>
                        <div class="form-group" style="margin: 0">
                          <input type="text" name="amount[]" placeholder="Amount" class="form-control">
                        </div>
                      </td>
                      <td class="text-right">
                        <div class="btn-group">
                          <input type="hidden" name="id">
                          <button class="btn-primary btn btn-xs save-inventory-btn" type="button"><i
                              class="fa fa-floppy-o"></i>
                            Save
                          </button>
                          <button class="btn-danger btn btn-xs cancel-inventory-edit-btn" type="button"><i
                              class="fa fa-minus-circle"></i> Cancel
                          </button>
                        </div>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

  @include('freems::templates.file-manager-modal')
@endsection

@push('scripts')
  <script>
    $(function () {
      $('.add-inventory-btn').click(function () {
        var viewModeRow = $('.inventory-row-view-mode.template').clone().removeClass('template');
        var editModeRow = $('.inventory-row-edit-mode.template').clone().removeClass('template');
        $('.product-inventory-table tbody').prepend(editModeRow).prepend(viewModeRow);
        editModeRow.show().find('input[name="size[]"]').focus();
        initProductInventoryRowEvents();
      });

      initProductInventoryRowEvents();

      function initProductInventoryRowEvents() {
        $('.edit-inventory-btn').unbind('click').click(function () {
          var row = $(this).closest('.inventory-row-view-mode');
          row.hide();
          row.next().show().find('input[name="size[]"]').focus();
        });

        $('.cancel-inventory-edit-btn').unbind('click').click(function () {
          var row = $(this).closest('.inventory-row-edit-mode');
          row.hide();
          row.prev().show();
        });

        $('.save-inventory-btn').unbind('click').click(function () {
          var row = $(this).closest('.inventory-row-edit-mode');
          var viewModeRow = row.prev();
          var data = {
            size: row.find('input[name="size[]"]').val(),
            color: row.find('select[name="color[]"] option:selected').text(),
            price: row.find('input[name="price[]"]').val(),
            stock_price: row.find('input[name="stock_price[]"]').val(),
            weight: row.find('input[name="weight[]"]').val(),
            amount: row.find('input[name="amount[]"]').val()
          };

          if (!data.size || !data.color || !data.price || !data.stock_price || !data.weight || !data.amount) {
            Modal.show({
              body: 'All fields are required!',
              withoutYes: true,
              no: 'Close'
            });
            return;
          }

          for (var p in  data) {
            if (data.hasOwnProperty(p)) {
              viewModeRow.find('[data-prop="' + p + '"]').text(data[p]);
            }
          }
          row.find('.cancel-inventory-edit-btn').click();
        });

        $('.delete-inventory-btn').unbind('click').click(function () {
          var row = $(this).closest('.inventory-row-view-mode');

          Modal.show({
            body: 'Are you sure?',
            yesClass: 'btn-danger',
            yes: 'Delete',
            callback: function (btn) {
              Modal.hide();

              if (btn === 'yes') {
                row.next('.inventory-row-edit-mode').remove();
                row.remove();
              }
            }
          })
        });
      }


      var indexRoute = "{{ route('freems.file.index') }}";
      var fileManager = createFileManager({
        modal: true,
        fileType: 'image',
        indexRoute: indexRoute,
        uploadBehaviour: 'related',
        onSelect: function (files) {
          $.each(files, function (k, file) {
            $('.selected-image-inputs').append(
              $('<input>').attr({type: 'hidden', name: 'images[]', value: file.id})
            );
            $('.image-previews').append(
              $('<a>').attr({
                href: file.src,
                title: file.title,
                'data-gallery': 'article-images'
              }).addClass("image").append(
                $('<button>').addClass('btn btn-danger btn-circle').attr({
                  'data-id': file.id,
                  type: 'button'
                }).append($('<i>').addClass('fa fa-times')),
                $('<img>').attr({src: file.src, alt: file.title})
              ));
          });
          bindClickOnUnselect();
        }
      });

      $('.select-media-btn').click(function () {
        fileManager.show();
      });

      function bindClickOnUnselect() {
        $('.image-previews .image button').click(function (e) {
          e.stopPropagation();
          e.preventDefault();
          var id = $(this).data().id;
          var parent = $(this).parent();


          Modal.show({
            yesClass: 'btn-danger',
            body: 'Are you sure, you want to unselect file?',
            yes: 'Unselect',
            callback: function (btn) {
              Modal.hide();

              if (btn === 'yes') {
                $('.selected-image-inputs input[value="' + id + '"]').remove();
                parent.remove();
              }
            }
          });
        })
      }
    })
  </script>
@endpush
