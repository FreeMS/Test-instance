<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Influencer extends Model
{
  protected $fillable = [
    'instagram', 'blog', 'followers'
  ];

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function shops()
  {
    return $this->hasMany(Shop::class, 'influencer_id');
  }

  public function instagramFeed()
  {
    return $this->hasMany(InstagramFeed::class, 'influencer_id');
  }
}
