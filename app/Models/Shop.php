<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Shop extends Model
{
  use Sluggable;

  protected $fillable = [
    'title', 'sub_title', 'cover', 'cover_color'
  ];

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  public function influencer()
  {
    return $this->belongsTo(Influencer::class, 'influencer_id');
  }

  public function products()
  {
    return $this->belongsToMany(Product::class, 'shop_products', 'shop_id', 'product_id');
  }
}
