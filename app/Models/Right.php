<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Right extends Model
{
  const INSTAGRAM_ANALYZER = 'instagram.analyzer';

  const BRANDS_VIEW = 'brands.view';
  const BRANDS_INVITE = 'brands.invite';
  const BRANDS_MANAGE = 'brands.manage';

  const PRODUCTS_VIEW = 'products.view';
  const PRODUCTS_MANAGE = 'products.manage';

  const USERS_VIEW = 'users.view';
  const USERS_MANAGE = 'users.manage';

  public function users() {
    return $this->belongsToMany(User::class, 'user_rights', 'right_id', 'user_id');
  }
}
