<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductInventory extends Model
{
  protected $fillable = [
    'size', 'price', 'stock_price', 'weight', 'amount', 'ord'
  ];

  public function product()
  {
    return $this->belongsTo(Product::class, 'product_id');
  }

  public function color()
  {
    return $this->belongsTo(ProductColor::class, 'product_color_id');
  }
}
