<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SizeStandard extends Model
{
  protected $fillable = [
    'name'
  ];
}
