<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Brand extends Model
{
  use Sluggable;

  protected $fillable = [
    'image', 'name', 'link', 'website', 'summary', 'registration_token',
    'business_name', 'registration_number', 'iban', 'swift', 'registered'
  ];

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  public function products()
  {
    return $this->hasMany(Product::class, 'brand_id');
  }

  public function addresses()
  {
    return $this->hasMany(Address::class, 'brand_id');
  }

  public function representatives()
  {
    return $this->hasMany(BrandRepresentative::class, 'brand_id');
  }
}
