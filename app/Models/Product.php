<?php

namespace App\Models;

use App\User;
use FreeMS\Models\File;
use FreeMS\Models\Folder;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
  use Sluggable;

  public $sortableFields = [
    'name' => 'product_name',
    'amount' => 'amount'
  ];

  public $searchableFields = [
    'name' => ['field' => 'products.name', 'type' => 'contains'],
    'size' => ['field' => 'product_inventories.size', 'type' => 'contains'],
    'color' => ['field' => 'product_colors.name', 'type' => 'contains']
  ];

  protected $fillable = [
    'name', 'description'
  ];

  protected static function boot()
  {
    parent::boot();

    static::saved(function($product) {
      self::buildRelationsToFiles($product);
    });
  }

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function brand()
  {
    return $this->belongsTo(Brand::class, 'brand_id');
  }

  public function size_standard()
  {
    return $this->belongsTo(SizeStandard::class, 'size_standard_id');
  }

  public function currency()
  {
    return $this->belongsTo(Currency::class, 'currency_id');
  }

  public function weight_unit()
  {
    return $this->belongsTo(WeightUnit::class, 'weight_unit_id');
  }

  public function inventory()
  {
    return $this->hasMany(ProductInventory::class, 'product_id');
  }

  public function shops()
  {
    return $this->belongsToMany(Shop::class, 'shop_products', 'product_id', 'shop_id');
  }

  public function images()
  {
    return $this->morphToMany(File::class, 'fileable');
  }

  public function folders()
  {
    return $this->morphToMany(Folder::class, 'folderable');
  }


  private static function buildRelationsToFiles($product) {
    $product->load('images');
    $product->load('folders');

    $relatedFiles = $product->images;

    if(count($relatedFiles)) {
      $folder = self::getFolder($product);
      $tmpFolder = Folder::where('name', '.tmp')->where('parent_id', null)->first();

      foreach($relatedFiles as $relatedFile) {
        if(!$folder->files->contains($relatedFile)) {
          $folder->files()->attach($relatedFile);
        }

        if($tmpFolder && $tmpFolder->files->contains($relatedFile)) {
          $tmpFolder->files()->detach($relatedFile);
        }
      }
    }
  }

  private static function getFolder($product)
  {
    if($product->folders->count()) {
      return $product->folders[0];
    } else {
      $productsFolder = Folder::where('name', 'Products')->where('parent_id', null)->first();
      if(!$productsFolder) {
        $productsFolder = new Folder(['name' => 'Products']);
        $productsFolder->save();
      }

      $folder = new Folder(['name' => $product->name]);
      $productsFolder->appendNode($folder);
      $folder->save();
      $product->folders()->attach($folder);
      return $folder;
    }
  }
}
