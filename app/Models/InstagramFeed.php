<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class InstagramFeed extends Model
{
  protected $fillable = [
    'image', 'link'
  ];

  public function influencer()
  {
    return $this->belongsTo(Influencer::class, 'influencer_id');
  }
}
