<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductInventory\CreateProductInventory;
use App\Http\Requests\ProductInventory\DeleteProductInventory;
use App\Http\Requests\ProductInventory\UpdateProductInventory;
use App\Models\Product;
use App\Models\ProductInventory;
use App\Models\Right;
use Illuminate\Support\Str;

class ProductInventoryController extends Controller
{
  public function store(CreateProductInventory $request)
  {
    $productInventory = new ProductInventory([
      'size' => $request->input('size'),
      'price' => $request->input('price'),
      'stock_price' => $request->input('stock_price'),
      'weight' => $request->input('weight'),
      'amount' => $request->input('amount'),
      'ord' => 0
    ]);
    $productInventory->product()->associate($request->input('product_id'));
    $productInventory->color()->associate($request->input('color'));
    $productInventory->save();

    $productInventory->load('color');

    return [ 'success' => true, 'productInventory' => $productInventory ];
  }

  public function update(UpdateProductInventory $request, ProductInventory $productInventory)
  {
    $color_id = $request->input('color');

    $productInventory->fill([
      'size' => $request->input('size'),
      'price' => $request->input('price'),
      'stock_price' => $request->input('stock_price'),
      'weight' => $request->input('weight'),
      'amount' => $request->input('amount')
    ]);

    $productInventory->color()->associate($color_id);
    $productInventory->save();

    $productInventory->load('color');

    return [ 'success' => true, 'productInventory' => $productInventory ];
  }

  public function destroy($productInventoryId)
  {
    $productInventory = ProductInventory::findOrFail($productInventoryId);
    return [ 'deleted' => $productInventory->delete() ];
  }
}
