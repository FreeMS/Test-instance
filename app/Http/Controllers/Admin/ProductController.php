<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Product\CreateProduct;
use App\Http\Requests\Product\UpdateProduct;
use FreeMS\Services\Product\ProductService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
  protected $productService;

  public function __construct(ProductService $productService){
      $this->productService = $productService;
  }

  public function index(Request $request) {
    $query = $request->input();
    $paginatedInventory = $this->productService->getList($query);

    return view('admin.products.index', [
      'inventory' => $paginatedInventory->appends(Input::except('page')),
      'query' => $query,
      'full_url' => $request->fullUrl()
    ]);
  }

  public function create() {
    $relationData = $this->productService->getRelationData();
    return view('admin.products.create', $relationData);
  }

  public function store(CreateProduct $request) {
    $this->productService->createProduct($request->input());

    \Session::flash('status', 'Product successfully added');
    return redirect()->route('admin.products.index');
  }

  public function edit($productId) {
    $relationData = $this->productService->getRelationData();
    $relationData['product'] = $this->productService->getById($productId);
    return view('admin.products.edit', $relationData);
  }

  public function update($productId, UpdateProduct $request) {
    $data = $request->input();
    $data['id'] = $productId;
    $this->productService->updateProduct($data);

    $relationData = $this->productService->getRelationData();
    $relationData['product'] = $this->productService->getById($productId);

    \Session::flash('status', 'Product info successfully updated');
    return redirect()->route('admin.products.edit', $productId);
  }

  public function destroy($productId) {
    $this->productService->deleteProduct($productId);

    \Session::flash('status', 'Product successfully deleted');
    return redirect()->route('admin.products.index');
  }
}
