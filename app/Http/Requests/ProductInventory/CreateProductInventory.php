<?php

namespace App\Http\Requests\ProductInventory;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductInventory extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'size' => 'required|string|max:32',
      'color' => 'required|integer|exists:product_colors,id',
      'price' => 'numeric|required',
      'stock_price' => 'numeric|required',
      'weight' => 'numeric|required',
      'amount' => 'integer|required',
      'product_id' => 'integer|required|exists:products,id',
    ];
  }
}
