<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProduct extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => 'required|string|max:255',
      'description' => 'required|string',
      'currency_id' => 'integer|required|exists:currencies,id',
      'size_standard_id' => 'integer|required|exists:size_standards,id',
      'weight_unit_id' => 'integer|required|exists:weight_units,id'
    ];
  }
}
