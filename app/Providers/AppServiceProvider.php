<?php

namespace App\Providers;

use App\Http\Controllers\Admin\ProductController;
use App\Models\Product;
use FreeMS\Repositories\Product\ProductRepository;
use FreeMS\Services\Product\ProductService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->when(ProductController::class)
        ->needs(ProductService::class)
        ->give(function () {
          $productRepository = new ProductRepository(Product::class);
          return new ProductService($productRepository);
        });
    }
}
